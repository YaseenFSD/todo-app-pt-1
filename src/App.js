import React, { useState } from "react";
import todosList from "./todos.json";
import TodoList from "./components/TodoList";
import { v4 } from "uuid";

function App(props) {

  const [todos, setTodos] = useState(todosList)
  const [typedText, setText] = useState("")



  const handleTextChange = (event) => {
    setText(event.target.value)


  }

  const clickEnter = (event) => {
    if (event.key === "Enter" && event.target.value !== "") {
      let newTodo = {
        "userId": 1,
        "id": v4(),
        "title": typedText,
        "completed": false
      }

      let newTodos = [...todos]
      newTodos.push(newTodo)

      setTodos((todos) => {
        return newTodos
      })
      // console.log(typedText)
      setText("")
      event.target.value = ""
    }
  }

  const handleCompleted = (todoId) => {
    for (let i = 0; i < todos.length; i++) {
      if (todos[i].id === todoId) {
        setTodos((todos) => {
          let opposite = !(todos[i].completed)
          let copyTodos = [...todos]
          copyTodos.splice(i, 1, { ...copyTodos[i], "completed": opposite })
          return copyTodos
        }
        )
      }
    }
    return "Not found"
  }

  const handleDelete = (todoId) => {
    for (let i = 0; i < todos.length; i++) {

      if (todos[i].id === todoId) {
        setTodos((todos) => {
          let copyTodos = [...todos]
          copyTodos.splice(i, 1)
          return copyTodos
        })
      }

    }

  }

  const clearCompleted = () => {
    // console.log("clearing")
    let copyTodos = [...todos]
    copyTodos = copyTodos.filter((currentTodo)=> !currentTodo.completed)
    // console.log({copyTodos})
    setTodos(()=> copyTodos)
  }


  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input className="new-todo" onKeyDown={clickEnter} onChange={handleTextChange} placeholder="What needs to be done?" autofocus />
      </header>
      <TodoList todos={todos} handleCompleted={handleCompleted} handleDelete={handleDelete} />
      <footer className="footer">
        <span className="todo-count">
          <strong>0</strong> item(s) left
          </span>
        <button onClick={() => clearCompleted()} className="clear-completed" >Clear completed</button>
      </footer>
    </section>
  );
}


export default App;
