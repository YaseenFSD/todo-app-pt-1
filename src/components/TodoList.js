import React from "react"
import TodoItem from "./TodoItem"

export default function TodoList (props){
    return (
        <section className="main">
          <ul className="todo-list">
            {props.todos.map((todo) => (
              <TodoItem title={todo.title} handleCompleted={props.handleCompleted} handleDelete={props.handleDelete} completed={todo.completed} key={todo.id} id={todo.id} />
            ))}
          </ul>
        </section>
      );
  }
  