import React from "react"



export default function TodoItem(props) {
    return (
        <li className={props.completed ? "completed" : ""}>
            <div className="view">
                <input className="toggle" type="checkbox" onClick={() => props.handleCompleted(props.id)} checked={props.completed} />

                <label>{props.title}</label>
                <button className="destroy" onClick={()=> props.handleDelete(props.id)} />
            </div>
        </li>
    );
}
